DROP SCHEMA IF EXISTS bl_dmav_einzelobjekte CASCADE;
DROP SCHEMA IF EXISTS bl_dmav_einzelobjekte_test CASCADE;
DROP SCHEMA IF EXISTS bl_dmav_grundstuecke CASCADE;
DROP SCHEMA IF EXISTS bl_dmav_grundstuecke_test CASCADE;
DROP SCHEMA IF EXISTS bl_dmav_dienstbarkeiten CASCADE;

CREATE EXTENSION IF NOT EXISTS "uuid-ossp";