-- create postgis extensions
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

-- reset t_id
ALTER SEQUENCE bl_dmav_dienstbarkeiten.t_ili2db_seq RESTART WITH 1;

-- create dataset information
INSERT INTO bl_dmav_dienstbarkeiten.t_ili2db_dataset (t_id, datasetname) VALUES
(nextval('bl_dmav_dienstbarkeiten.t_ili2db_seq'), 'Testdatensatz');

-- create basket information
INSERT INTO bl_dmav_dienstbarkeiten.t_ili2db_basket (t_id, dataset, topic, t_ili_tid, attachmentkey, domains) VALUES
(
        nextval('bl_dmav_dienstbarkeiten.t_ili2db_seq'),
        (SELECT t_id FROM bl_dmav_dienstbarkeiten.t_ili2db_dataset),
        'ch_bl_agi_dmav_bl_dienstbarkeiten_v1_0.Dienstbarkeiten',
        uuid_generate_v4(),
        'dummy_string',
        ''
);

INSERT INTO bl_dmav_dienstbarkeiten.wegrecht
(t_basket, t_ili_tid, geometrie)
VALUES
(
    (SELECT t_id FROM bl_dmav_dienstbarkeiten.t_ili2db_basket WHERE topic = 'ch_bl_agi_dmav_bl_dienstbarkeiten_v1_0.Dienstbarkeiten'),
    uuid_generate_v4(),
    ST_ForceCurve(ST_GeomFromText('LINESTRING(2612970.0 1259867.7, 2612987.0 1259871.8)', 2056))
)
