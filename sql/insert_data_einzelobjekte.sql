-- create postgis extensions
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

-- reset t_id
ALTER SEQUENCE bl_dmav_einzelobjekte.t_ili2db_seq RESTART WITH 1;

-- create dataset information
INSERT INTO bl_dmav_einzelobjekte.t_ili2db_dataset (t_id, datasetname) VALUES
(nextval('bl_dmav_einzelobjekte.t_ili2db_seq'), 'Testdatensatz');

-- create basket information
INSERT INTO bl_dmav_einzelobjekte.t_ili2db_basket (t_id, dataset, topic, t_ili_tid, attachmentkey, domains) VALUES
(
        nextval('bl_dmav_einzelobjekte.t_ili2db_seq'),
        (SELECT t_id FROM bl_dmav_einzelobjekte.t_ili2db_dataset),
        'ch_bl_agi_dmav_bl_einzelobjekte_v1_0.EinzelobjekteBL',
        uuid_generate_v4(),
        'dummy_string',
        ''
);

-- EONachfuehrung "Bahnhof Dornach-Arlesheim"
INSERT INTO bl_dmav_einzelobjekte.eonachfuehrung
(t_basket, nbident, identifikator, beschreibung, perimeter, gueltigereintrag)
VALUES
(
    (SELECT t_id FROM bl_dmav_einzelobjekte.t_ili2db_basket WHERE topic = 'ch_bl_agi_dmav_bl_einzelobjekte_v1_0.EinzelobjekteBL'),
    'BL0200002763',
    'Ident-00001',
    'Bahnhof Dornach-Arlesheim',
    ST_ForceCurve(ST_GeomFromText('POLYGON((2612930 1260000, 2613040 1260000, 2613040 1259860, 2612930 1259860, 2612930 1260000))', 2056)),
    '2023-08-29'
);

-- Einzelobjekt "Perrondach"
INSERT INTO bl_dmav_einzelobjekte.einzelobjekt
(t_basket, t_type, t_ili_tid, qualitaetsstandard, einzelobjektart, objektstatus, entstehung)
VALUES
(
    (SELECT t_id FROM bl_dmav_einzelobjekte.t_ili2db_basket WHERE topic = 'ch_bl_agi_dmav_bl_einzelobjekte_v1_0.EinzelobjekteBL'),
    'ch_bl_gkt_v1_0einzelobjektebl_einzelobjekt',
    uuid_generate_v4(),
    'AV93',
    'Unterstand.Perrondach',
    'real',
    (SELECT max(t_id) FROM bl_dmav_einzelobjekte.eonachfuehrung)
);

-- Flaechenelement "Unterstand.Perrondach"
INSERT INTO bl_dmav_einzelobjekte.flaechenelement (t_basket, geometrie, einzelobjekt_flaechenelement) VALUES
(
    (SELECT t_id FROM bl_dmav_einzelobjekte.t_ili2db_basket WHERE topic = 'ch_bl_agi_dmav_bl_einzelobjekte_v1_0.EinzelobjekteBL'),
    ST_ForceCurve(ST_GeomFromText('POLYGON((2612990 1259939, 2612994 1259939, 2612999 1259875, 2612994 1259875, 2612990 1259939))', 2056)),
    (SELECT max(t_id) FROM bl_dmav_einzelobjekte.einzelobjekt)
);

-- Einzelobjekt "Unterstand.Unterstand"
INSERT INTO bl_dmav_einzelobjekte.einzelobjekt
(t_basket, t_type, t_ili_tid, qualitaetsstandard, einzelobjektart, objektstatus, entstehung)
VALUES
(
    (SELECT t_id FROM bl_dmav_einzelobjekte.t_ili2db_basket WHERE topic = 'ch_bl_agi_dmav_bl_einzelobjekte_v1_0.EinzelobjekteBL'),
    'ch_bl_gkt_v1_0einzelobjektebl_einzelobjekt',
    uuid_generate_v4(),
    'AV93',
    'Unterstand.Unterstand',
    'real',
    (SELECT max(t_id) FROM bl_dmav_einzelobjekte.eonachfuehrung)
);

-- Flaechenelement "Unterstand.Unterstand"
INSERT INTO bl_dmav_einzelobjekte.flaechenelement (t_basket, geometrie, einzelobjekt_flaechenelement) VALUES
(
    (SELECT t_id FROM bl_dmav_einzelobjekte.t_ili2db_basket WHERE topic = 'ch_bl_agi_dmav_bl_einzelobjekte_v1_0.EinzelobjekteBL'),
    ST_ForceCurve(ST_GeomFromText('POLYGON((2612971 1259861, 2612993 1259850, 2612996 1259810, 2612975 1259806, 2612971 1259861))', 2056)),
    (SELECT max(t_id) FROM bl_dmav_einzelobjekte.einzelobjekt)
);

