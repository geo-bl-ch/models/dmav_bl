-- create postgis extensions
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

-- reset t_id
ALTER SEQUENCE bl_dmav_grundstuecke.t_ili2db_seq RESTART WITH 1;

-- create dataset information
INSERT INTO bl_dmav_grundstuecke.t_ili2db_dataset (t_id, datasetname) VALUES
(nextval('bl_dmav_grundstuecke.t_ili2db_seq'), 'Testdatensatz');

-- create basket information
INSERT INTO bl_dmav_grundstuecke.t_ili2db_basket (t_id, dataset, topic, t_ili_tid, attachmentkey, domains) VALUES
(
        nextval('bl_dmav_grundstuecke.t_ili2db_seq'),
        (SELECT t_id FROM bl_dmav_grundstuecke.t_ili2db_dataset),
        'ch_bl_agi_dmav_bl_grundstuecke_v1_0.GrundstueckeBL',
        uuid_generate_v4(),
        'dummy_string',
        ''
);


-- GSNachführung "Bahnhof Dornach-Arlesheim"
INSERT INTO bl_dmav_grundstuecke.gsnachfuehrung
(t_basket, nbident, identifikator, beschreibung, perimeter, mutationsart, gueltigereintrag, grundbucheintrag)
VALUES
(
    (SELECT t_id FROM bl_dmav_grundstuecke.t_ili2db_basket WHERE topic = 'ch_bl_agi_dmav_bl_grundstuecke_v1_0.GrundstueckeBL'),
    'BL0200002763',
    'Ident-00001',
    'Bahnhof Dornach-Arlesheim',
    ST_ForceCurve(ST_GeomFromText('POLYGON((2612930 1260000, 2613040 1260000, 2613040 1259850, 2612930 1259850, 2612930 1260000))', 2056)),
    'Normal',
    '2023-08-29',
    '2023-08-29'
);


-- Grenzpunkt "Hoheitsgrenzpunkt"
INSERT INTO bl_dmav_grundstuecke.Grenzpunkt
(t_basket, t_type, t_ili_tid, nbident, nummer, geometrie, hoehengeometrie, lagegenauigkeit, istlagezuverlaessig,
hoehengenauigkeit, isthoehenzuverlaessig, punktzeichen, isthoheitsgrenzpunkt, isthoheitsgrenzsteinalt, istexaktdefiniert,
symbolori, SteinNummer, entstehung)
VALUES
(
    (SELECT t_id FROM bl_dmav_grundstuecke.t_ili2db_basket WHERE topic = 'ch_bl_agi_dmav_bl_grundstuecke_v1_0.GrundstueckeBL'),
    'ch_bl_gck_v1_0grundstueckebl_grenzpunkt',
    uuid_generate_v4(),
    'BL0200002763',
    'BL0000000001',
    ST_GeomFromText('POINT(2612963.2 1259863.8)', 2056),
    294.5,
    0.01,
    true,
    0.01,
    true,
    'Stein',
    true,
    false,
    true,
    0,
    'BL0000000001',
    (SELECT max(t_id) FROM bl_dmav_grundstuecke.gsnachfuehrung)
),(
    (SELECT t_id FROM bl_dmav_grundstuecke.t_ili2db_basket WHERE topic = 'ch_bl_agi_dmav_bl_grundstuecke_v1_0.GrundstueckeBL'),
    'ch_bl_gck_v1_0grundstueckebl_grenzpunkt',
    uuid_generate_v4(),
    'BL0200002763',
    'BL0000000002',
    ST_GeomFromText('POINT(2612960.3 1259863.7)', 2056),
    294.5,
    0.01,
    true,
    0.01,
    true,
    'Stein',
    true,
    false,
    true,
    0,
    null,
    (SELECT max(t_id) FROM bl_dmav_grundstuecke.gsnachfuehrung)
),(
    (SELECT t_id FROM bl_dmav_grundstuecke.t_ili2db_basket WHERE topic = 'ch_bl_agi_dmav_bl_grundstuecke_v1_0.GrundstueckeBL'),
    'ch_bl_gck_v1_0grundstueckebl_grenzpunkt',
    uuid_generate_v4(),
    'BL0200002763',
    'BL0000000003',
    ST_GeomFromText('POINT(2612970.0 1259867.5)', 2056),
    294.5,
    0.01,
    true,
    0.01,
    true,
    'Bolzen',
    false,
    false,
    true,
    0,
    null,
    (SELECT max(t_id) FROM bl_dmav_grundstuecke.gsnachfuehrung)
);

-- Errors
INSERT INTO bl_dmav_grundstuecke.Grenzpunkt
(t_basket, t_type, t_ili_tid, nbident, nummer, geometrie, hoehengeometrie, lagegenauigkeit, istlagezuverlaessig,
hoehengenauigkeit, isthoehenzuverlaessig, punktzeichen, isthoheitsgrenzpunkt, isthoheitsgrenzsteinalt, istexaktdefiniert,
symbolori, SteinNummer, entstehung)
VALUES
(
    (SELECT t_id FROM bl_dmav_grundstuecke.t_ili2db_basket WHERE topic = 'ch_bl_agi_dmav_bl_grundstuecke_v1_0.GrundstueckeBL'),
    'ch_bl_gck_v1_0grundstueckebl_grenzpunkt',
    uuid_generate_v4(),
    'BL0200002763',
    'BL0000000004',
    ST_GeomFromText('POINT(2612963.2 1259863.8)', 2056),
    294.5,
    0.01,
    true,
    0.01,
    true,
    'Stein',
    false,
    false,
    true,
    0,
    null,
    (SELECT max(t_id) FROM bl_dmav_grundstuecke.gsnachfuehrung)
);
